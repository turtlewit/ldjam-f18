extends Area2D

func _on_Area2D_body_entered(body):
	if "Lemmings" in body.get_groups():
		body.Die()
