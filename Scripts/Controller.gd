extends Node

enum {STATE_PAUSED, STATE_MENU, STATE_IN_GAME}
enum {MODE_NORMAL, MODE_KILL, MODE_JUMP, MODE_SELECT}
var state = STATE_IN_GAME
var mode = MODE_NORMAL
var state_funcs = {
	STATE_PAUSED: funcref(self, "Paused"),
	STATE_MENU: funcref(self, "Menu"),
	STATE_IN_GAME: funcref(self, "InGame")
}
var mode_funcs = {
	MODE_NORMAL: funcref(self, "ActionNormal"),
	MODE_KILL: funcref(self, "ActionKill"),
	MODE_JUMP: funcref(self, "ActionJump"),
	MODE_SELECT: funcref(self, "ActionSelect")
}

const jumpForce = 100

var selectionPrefab = load("res://Prefabs/SelectionArea.tscn")
onready var selection = selectionPrefab.instance()
onready var selectionCol = selection.get_node("CollisionShape2D")
onready var selectionRect = selection.get_node("NinePatchRect")
var selecting = false

var cursorPrefab = load("res://Prefabs/Cursor.tscn")


var uiPrefab = load("res://Prefabs/UI.tscn")
onready var ui = uiPrefab.instance()
onready var normalButton = ui.get_node("In-Game/Normal")
onready var selectButton = ui.get_node("In-Game/Select")
onready var killButton = ui.get_node("In-Game/Kill")
onready var jumpButton = ui.get_node("In-Game/Jump")
onready var container = ui.get_node("In-Game")

onready var cursor = ui.get_node("Cursor")
onready var cursorAnimator = cursor.get_node("Cursor/AnimationPlayer")
func cursorInUI():
	var mpos = get_viewport().get_mouse_position()
	return mpos.x < (container.rect_position.x + container.rect_size.x) and mpos.x > container.rect_position.x and mpos.y < (container.rect_position.y + container.rect_size.y) and mpos.y > container.rect_position.y 


var spawned_lemmings = []
var dead_lemmings = []
var selected_lemmings = []
var mouse_over_lemmings = []

const levels = [
	preload("res://Scenes/Level 1.tscn"),
	preload("res://Scenes/Level 2.tscn"),
	preload("res://Scenes/Level 3.tscn"),
	preload("res://Scenes/Level 4.tscn")
]
var currentLevel = 0

func _ready():
	add_child(selection)
	add_child(ui)
	Input.set_mouse_mode(1)
	cursorAnimator.play("Normal")
	
	normalButton.connect("pressed", self, "_on_normal_button_pressed")
	selectButton.connect("pressed", self, "_on_select_button_pressed")
	killButton.connect("pressed", self, "_on_kill_button_pressed")
	jumpButton.connect("pressed", self, "_on_jump_button_pressed")
	
	container.connect("mouse_entered", self, "_on_mouse_enter_ui")
	container.connect("mouse_exited", self, "_on_mouse_exit_ui")

func _process(delta):
	cursor.rect_position = get_viewport().get_mouse_position()
	if mode == MODE_NORMAL and cursorAnimator.current_animation != "Normal":
		cursorAnimator.play("Normal")
	if mode == MODE_KILL and cursorAnimator.current_animation != "Kill":
		cursorAnimator.play("Kill")
	if mode == MODE_JUMP and cursorAnimator.current_animation != "Jump":
		cursorAnimator.play("Jump")
	if mode == MODE_SELECT and cursorAnimator.current_animation != "Select":
		cursorAnimator.play("Select")

func _physics_process(delta):
	state_funcs[state].call_func(delta)

func InGame(delta):
	if Input.is_action_just_pressed("mouse1"):
		if not cursorInUI():
			mode_funcs[mode].call_func()
	if selecting:
		if not cursorInUI():
			if Input.is_action_pressed("mouse1"):
				selectionCol.shape.extents = (get_viewport().get_mouse_position() - selection.position) / 2
				selectionCol.position = selectionCol.shape.extents
				var size = selectionCol.shape.extents * 2
				if size.x < 0:
					selectionRect.rect_scale.x = -1
					size.x -= 1
				else:
					selectionRect.rect_scale.x = 1
				if size.y < 0:
					selectionRect.rect_scale.y = -1
					size.y -= 1
				else:
					selectionRect.rect_scale.y = 1
				selectionRect.rect_size = Vector2(abs(size.x), abs(size.y))
				var overlappingBodies = selection.get_overlapping_bodies()
			if Input.is_action_just_released("mouse1"):
				for lemming in selected_lemmings:
					lemming.get_node("Highlight").visible = false
				selected_lemmings = []
				var overlappingBodies = selection.get_overlapping_bodies()
				for body in overlappingBodies:
					if "Lemmings" in body.get_groups() and body.selectable:
						selected_lemmings.append(body)
						body.get_node("Highlight").visible = true
				selection.position = Vector2(0,0)
				selectionCol.shape.extents = Vector2(0, 0)
				selectionCol.position = Vector2(0,0)
				selectionRect.visible = false
				selecting = false
	if Input.is_action_just_pressed("stop_all_lemmings"):
		StopAllLemmings()
	if Input.is_action_just_pressed("start_all_lemmings"):
		StartAllLemmings()
	if Input.is_action_just_pressed("kill_mode"):
		mode = MODE_KILL
	if Input.is_action_just_pressed("normal_mode"):
		mode = MODE_NORMAL
	if Input.is_action_just_pressed("jump_mode"):
		mode = MODE_JUMP
	if Input.is_action_just_pressed("select"):
		mode = MODE_SELECT
		
func StopAllLemmings():
	for lemming in selected_lemmings:
		if lemming.onGround:
			lemming.stop = true

func StartAllLemmings():
	for lemming in selected_lemmings:
		lemming.stop = false

func Paused(delta):
	pass

func Menu(delta):
	pass

func ActionNormal():
	if len(mouse_over_lemmings) > 0:
		if mouse_over_lemmings[0].onGround:
			mouse_over_lemmings[0].stop = not mouse_over_lemmings[0].stop
	else:
		for lemming in selected_lemmings:
			if get_viewport().get_mouse_position().x - lemming.position.x > 0:
				lemming.direction = Vector2(1, 0)
			else:
				lemming.direction = Vector2(-1, 0)
			lemming.stop = false

func ActionKill():
	if len(mouse_over_lemmings) > 0:
		mouse_over_lemmings[0].Die()
		mouse_over_lemmings.pop_front()

func ActionJump():
	if len(mouse_over_lemmings) > 0:
		var lemming = mouse_over_lemmings[0]
		if lemming.onGround:
			lemming.velocity = lemming.move_and_slide(Vector2(lemming.velocity.x, -jumpForce), Vector2(0, -1))

func ActionSelect():
	selection.position = get_viewport().get_mouse_position()
	selectionRect.visible = true
	selecting = true

func mouse_entered_lemming(lemming):
	if not lemming in mouse_over_lemmings:
		mouse_over_lemmings.append(lemming)

func mouse_exited_lemming(lemming):
	if lemming in mouse_over_lemmings:
		mouse_over_lemmings.erase(lemming)

func _on_normal_button_pressed():
	mode = MODE_NORMAL
func _on_select_button_pressed():
	mode = MODE_SELECT
func _on_kill_button_pressed():
	mode = MODE_KILL
func _on_jump_button_pressed():
	mode = MODE_JUMP

func Spawn(where, what):
	var whatInstance = what.instance()
	get_node("/root/Scene").add_child(whatInstance)
	whatInstance.position = where.position
	spawned_lemmings.append(whatInstance)

func Win():
	ui.get_node("Win").visible = true

func Begin():
	get_tree().change_scene_to(levels[0])
	container.visible = true

func Continue():
	spawned_lemmings = []
	dead_lemmings = []
	ui.get_node("Win").visible = false
	if len(levels) - 1 >= currentLevel + 1:
		currentLevel += 1
		get_tree().change_scene_to(levels[currentLevel])
	else:
		get_tree().change_scene("res://Scenes/MainMenu.tscn")