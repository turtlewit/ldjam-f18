extends Area2D

export(int) var lemmingsNeeded

var lemmingsWon = 0

onready var label = $Label

func _ready():
	label.text = "%d" % (lemmingsNeeded - lemmingsWon)

func _process(delta):
	pass


func _on_Goal_body_entered(body):
	if "Lemmings" in body.get_groups():
		body.StartWin()
		body.position.x = position.x
		lemmingsWon += 1
		label.text = "%d" % (lemmingsNeeded - lemmingsWon)
	if lemmingsWon >= lemmingsNeeded:
		Controller.Win()
