extends KinematicBody2D

var gravity = Vector2(0, 98)
var velocity = Vector2(0, 0)
export(float) var speed
export(Vector2) var direction = Vector2(1, 0)
export var stop = false
var onGround = false
onready var groundCollider = $Area2D
onready var sprite = $Sprite
onready var animator = $Sprite/AnimationPlayer

var selectable = true

enum {TYPE_NORMAL, TYPE_DEAD, TYPE_WIN}
var type = TYPE_NORMAL

func _ready():
	connect("mouse_entered", self, "_mouse_entered_lemming")
	connect("mouse_exited", self, "_mouse_exited_lemming")
	animator.play("Walk")

func _process(delta):
	if direction.x > 0:
		sprite.flip_h = false
	else:
		sprite.flip_h = true
	if type == TYPE_NORMAL:
		if stop and animator.current_animation != "Idle":
			animator.play("Idle")
		if not stop and not type == TYPE_DEAD:
			if onGround and animator.current_animation != "Walk":
				animator.play("Walk")
			elif not onGround and animator.current_animation != "Falling":
				animator.play("Falling")
		

func _physics_process(delta):
	match type:
		TYPE_NORMAL:
			if onGround:
				GroundMove(delta)
			else:
				AirMove(delta)
			if stop:
				velocity.x = 0
			velocity = move_and_slide(velocity, Vector2(0, -1))
			onGround = len(groundCollider.get_overlapping_bodies()) > 0
		TYPE_DEAD:
			pass
		TYPE_WIN:
			if not animator.is_playing():
				queue_free()

func GroundMove(delta):
	if direction.x > 0:
		if velocity.x < speed:
			velocity.x += (100 * direction).x * delta
	else:
		if velocity.x > -speed:
			velocity.x += (100 * direction).x * delta

func AirMove(delta):
	if direction.x > 0:
		if velocity.x < speed:
			velocity.x += (100 * direction).x * delta
	else:
		if velocity.x > -speed:
			velocity.x += (100 * direction).x * delta
	ApplyGravity(delta)

func ApplyGravity(delta):
	velocity += gravity * delta

func Die():
	Controller.spawned_lemmings.erase(self)
	Controller.dead_lemmings.append(self)
	animator.play("Die")
	type = TYPE_DEAD
	$Collider.disabled = true
	$RigidBody2D.sleeping = false
	$RigidBody2D/CollisionShape2D.disabled = false
	selectable = false
	set_process(false)
	set_physics_process(false)

func StartWin():
	Controller.spawned_lemmings.erase(self)
	type = TYPE_WIN
	animator.play("Success")

func _mouse_entered_lemming():
	if selectable:
		Controller.mouse_entered_lemming(self)

func _mouse_exited_lemming():
	if selectable:
		Controller.mouse_exited_lemming(self)