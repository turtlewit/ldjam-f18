extends Sprite

export(PackedScene) var lemmingPrefab

export(float) var spawnInterval
export(int) var numberToSpawn
export(int) var numberToRespawn

var currentlySpawned = 0
var respawned = 0

var time = 0.0

func _ready():
	pass

func _process(delta):
	if time > spawnInterval:
		if currentlySpawned < numberToSpawn:
			Controller.Spawn(self, lemmingPrefab)
			time = 0.0
			currentlySpawned += 1
		elif len(Controller.dead_lemmings) > respawned and respawned < numberToRespawn:
			Controller.Spawn(self, lemmingPrefab)
			time = 0.0
			respawned += 1
	time += delta
